import java.util.Scanner;
public class LuckyCardGameApp{
	public static void main(String[] args){
		//create new deck and shuffle
		Deck myDeck = new Deck();
		myDeck.shuffle();
		//find out how mnay cards they would like to remove
		Scanner reader = new Scanner(System.in);
		System.out.println("How many cards do you wish to remove?");
		int remove = reader.nextInt();
		//print length of cards
		System.out.println(myDeck.length());
		//print length of cards - the amount asked to remove
		System.out.println(myDeck.length() - remove);
		//shuffle deck then print 
		myDeck.shuffle();
		System.out.println(myDeck);
	}
}